/**
 * How to use:
 * 1. Create an object named Setting__c
 * 2. Create a field on it called Value__c
 * 3. Ensure the most restrictive security is set up on this object so only admins can view/edit values.
 *
 * In the future, the object should be included as well in the repo.
 */
public without sharing class SettingsManager {
    /**
     * Return the namespace prefix or an empty string if there isn't one
     */
    public static String namespacePrefix {
        get {
            if (namespacePrefix == null) {
                String[] parts = String.valueOf(SettingsManager.class).split('\\.', 2);
                namespacePrefix = parts.size() == 2 ? parts[0] : '';
            }
            return namespacePrefix;
        }
        private set;
    }

    /**
     * Name of the settings object
     */
    @TestVisible static String SETTINGS_OBJECT_NAME = namespacePrefix == '' ? 'setting__c' : namespacePrefix + '__setting__c';

    /**
     * Holds the value cache so subsequent values of getValue don't run another query
     */
    static Map<String, String> valueCache = new Map<String, String>();

    /**
     * Holds the exception message in case the setting was not able to be inserted/updated
     */
    static String settingsInsertException;

    /**
     * Get a setting value by its name
     *
     * @param settingName Name of the setting
     * @param defaultValue In case the setting is not present, what should the default value be?
     *
     * @return Setting value if the setting is set, defaultValue otherwise
     */
    public static String getValue(String settingName, String defaultValue) {
        if (valueCache.containsKey(settingName)) {
            return valueCache.get(settingName);
        } else {
            // We put this in a dynamic SOQL so that we can deploy the class even if the Settings__c object doesn't
            // exist
            SObject[] settingObj = Database.query('SELECT Value__c FROM ' + SETTINGS_OBJECT_NAME + ' WHERE Name = :settingName');
            if (settingObj.size() == 0) {
                valueCache.put(settingName, defaultValue);
                return defaultValue;
            }
            String value = (String) settingObj[0].get('Value__c');
            valueCache.put(settingName, value);
            return value;
        }
    }

    /**
     * Sets a setting.
     *
     * @param settingName Name of the setting
     * @param value Value
     *
     * @return true if successful, false if an exception occurred. Use {@link SettingsManager#getException} for info
     */
    public static Boolean setValue(String settingName, String value) {
        // Uncache the value so that it's explicitly fetched next time
        valueCache.remove(settingName);

        SObject sObj = Schema.getGlobalDescribe().get(SETTINGS_OBJECT_NAME)?.newSObject();
        try {
            sObj.put('Name', settingName);
            sObj.put('Value__c', value);
            SObject[] settingObj = Database.query('SELECT Id FROM ' + SETTINGS_OBJECT_NAME + ' WHERE Name = :settingName');
            if (settingObj.size() == 0) {
                insert sObj;
            } else {
                sObj.put('Id', settingObj[0].Id);
                update sObj;
            }
        } catch (Exception e) {
            settingsInsertException = e.getMessage();
            return false;
        }

        return true;
    }

    /**
     * Returns the exception in case a setting was not able to be inserted.
     *
     * @return Exception message
     */
    public static String getException() {
        return settingsInsertException;
    }

    /**
     * Invocable method to get a setting from flows.
     *
     * @param settingNames List of setting names to get values for
     *
     * @return List of values in the same order as the settingNames were input (default is always null)
     */
    @InvocableMethod(Label='Get Setting Value')
    public static List<String> getValueInvocable(List<String> settingNames) {
        List<String> settingValues = new List<String>();
        for (String settingName : settingNames) {
            settingValues.add(getValue(settingName, null));
        }

        return settingValues;
    }

}