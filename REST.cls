/**
 * Created by rnavr on 01-04-2020.
 */

public class REST {
    String apiUrl;
    Boolean useAuth;
    Map<String, String> additionalHeaders;

    public enum RequestMethod {
        GET, POST, PUT, PATCH
    }

    public REST(String apiUrl) {
        this.apiUrl = apiUrl;
        this.useAuth = false;
        this.additionalHeaders = null;
    }

    public REST(String apiUrl, Boolean useAuth) {
        this.apiUrl = apiUrl;
        this.useAuth = useAuth;
        this.additionalHeaders = null;
    }

    public REST(String apiUrl, Boolean useAuth, Map<String, String> additionalHeaders) {
        this.apiUrl = apiUrl;
        this.useAuth = useAuth;
        this.additionalHeaders = additionalHeaders;
    }

    public RequestResult request(String endpoint, RequestMethod method, String body) {
        return request(endpoint, method, body, 'Authorization', '{!$Credential.Password}', 'application/json', null);
    }

    public RequestResult request(String endpoint, RequestMethod method, String body, String authKey, String authPass,
            String contentType, String contentLength) {
        HttpRequest req = new HttpRequest();
        req.setTimeout(60000);
        req.setEndpoint(apiUrl + endpoint);

        if (useAuth) {
            Utils.logVerbose('Using auth');
            req.setHeader(authKey, authPass);
        }
        if (additionalHeaders != null) {
            for (String key : additionalHeaders.keySet()) {
                req.setHeader(key, additionalHeaders.get(key));
            }
        }

        if (method == RequestMethod.GET) {
            req.setMethod('GET');
        }
        else if (method == RequestMethod.POST || method == RequestMethod.PUT || method == RequestMethod.PATCH) {
            req.setHeader('Content-Type', contentType);
            req.setHeader('Accept', '*/*');
            req.setMethod(method == RequestMethod.POST ? 'POST' : method == RequestMethod.PUT ? 'PUT' : 'PATCH');
            Utils.logVerbose('Request body: ' + body);
            req.setBody(body);
        }

        if (contentLength != null) {
            req.setHeader('Content-Length', contentLength);
        }

        Utils.logVerbose('Request: ' + apiUrl + endpoint);
        HttpResponse resp = new HttpResponse();
        try {
            resp = new Http().send(req);

            if (Utils.getTestFlag())
                throw new System.CalloutException('Generic error for test');
        } catch (System.CalloutException e) {
            Utils.error('Callout error: ' + e);
            RequestResult failResult = new RequestResult();
            failResult.isSuccess = false;
            failResult.response = '';
            failResult.errorCode = 0;
            failResult.errorMsg = e.getMessage();
            return failResult;
        }

        Utils.logVerbose('Response: ' + resp.toString());

        RequestResult requestResult = new RequestResult();
        if (resp.getStatusCode() >= 200 && resp.getStatusCode() < 300) {
            Utils.logVerbose('Response body: ' + resp.getBody());
            requestResult.response = resp.getBody();
            requestResult.isSuccess = true;
        }
        else {
            Utils.error('Request failed: ' + resp.getStatusCode() + resp.getBody());
            requestResult.isSuccess = false;
            requestResult.errorCode = resp.getStatusCode();
            requestResult.errorMsg = resp.getBody();
        }

        return requestResult;
    }

    public RequestResult get(String endpoint) {
        return request(endpoint, RequestMethod.GET, null);
    }

    public RequestResult post(String endpoint, String jsonBody) {
        return request(endpoint, RequestMethod.POST, jsonBody);
    }

    public RequestResult put(String endpoint, String jsonBody) {
        return request(endpoint, RequestMethod.PUT, jsonBody);
    }

    public RequestResult post(String endpoint, Map<String, Object> jsonMap) {
        String jsonBody = JSON.serialize(jsonMap);
        return post(endpoint, jsonBody);
    }

    public RequestResult put(String endpoint, Map<String, Object> jsonMap) {
        String jsonBody = JSON.serialize(jsonMap);
        return put(endpoint, jsonBody);
    }

    public RequestResult loggedPost(String endpoint, Map<String, Object> requestBody, Object relevantObject) {
        RequestResult postResult = post(endpoint, requestBody);
        if (!postResult.isSuccess) {
            Utils.error('POST Error ' + postResult.errorCode + ' on endpoint ' + endpoint + ', details will follow: ');
            Utils.error('Error message: ' + postResult.errorMsg);
            Utils.error('Relevant objects: ' + relevantObject);
        }
        return postResult;
    }

    public RequestResult loggedPut(String endpoint, Map<String, Object> requestBody, Object relevantObject) {
        RequestResult putResult = put(endpoint, requestBody);
        if (!putResult.isSuccess) {
            Utils.error('PUT Error ' + putResult.errorCode + ' on endpoint ' + endpoint + ', details will follow: ');
            Utils.error('Error message: ' + putResult.errorMsg);
            Utils.error('Relevant objects: ' + relevantObject);
        }
        return putResult;
    }

    public RequestResult loggedGet(String endpoint, Object relevantObject) {
        RequestResult getResult = get(endpoint);
        if (!getResult.isSuccess) {
            Utils.error('GET Error ' + getResult.errorCode + ' on endpoint ' + endpoint + ', details will follow: ');
            Utils.error('Error message: ' + getResult.errorMsg);
            Utils.error('Relevant objects: ' + relevantObject);
        }
        return getResult;
    }

    /**
    * Request result.
    */
    public class RequestResult {
        public String response { get; set; }
        public Boolean isSuccess { get; set; }
        public String errorMsg { get; set; }
        public Integer errorCode { get; set; }
    }

}