/**
 * This class provides a wrapper to format strings, as String.format() requires a List<String> as the second argument
 * which makes code unnecessarily long and string formatting difficult to type out.
 *
 * This provides interpolation for up to 8 parameters.
 *
 * Usage example:
 ** String x = Fmt.str('The order {0} has {1} error at {2}', order.Id, 5, System.now());
 *
 * Usage:
 *  Fmt.str(templateString, arg0, arg1, ...);
 *
 * Returns a String with args interpolated into templateString.
 *
 * Refer Apex doc for more info:
 * https://developer.salesforce.com/docs/atlas.en-us.apexref.meta/apexref/apex_methods_system_string.htm#apex_System_String_format
 */
public class Fmt {
    public static String str(String template, Object arg0) {
        return String.format(template, new List<Object>{arg0});
    }

    public static String str(String template, Object arg0, Object arg1) {
        return String.format(template, new List<Object>{arg0, arg1});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2) {
        return String.format(template, new List<Object>{arg0, arg1, arg2});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2, Object arg3) {
        return String.format(template, new List<Object>{arg0, arg1, arg2, arg3});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2, Object arg3, Object arg4) {
        return String.format(template, new List<Object>{arg0, arg1, arg2, arg3, arg4});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5) {
        return String.format(template, new List<Object>{arg0, arg1, arg2, arg3, arg4, arg5});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5,
            Object arg6) {
        return String.format(template, new List<Object>{arg0, arg1, arg2, arg3, arg4, arg5, arg6});
    }

    public static String str(String template, Object arg0, Object arg1, Object arg2, Object arg3, Object arg4, Object arg5,
            Object arg6, Object arg7) {
        return String.format(template, new List<Object>{arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7});
    }
}