public class Utils {
    /**
     * Is the current org a sandbox instance?
     */
    public static Boolean isSandbox;

    /**
     * Flag to purposely fail a unit test, use with {@link Utils#setTestFlag()} and {@link Utils#getTestFlag()}
     */
    public static Integer failForTest;

    /**
     * Buffer for errors that will be emailed
     */
    public static String emailErrors;

    /**
     * Buffer for debug messages that will be emailed
     */
    public static String emailDebug;

    /**
     * Flag to track whether we need to send the error email or not
     */
    public static Boolean sendEmailOfErrors;

    /**
     * Is person account support enabled on the current instance?
     */
    public static Boolean isPersonAccountsEnabled;

    /**
     * Is multi currency support enabled on the current instance?
     */
    public static Boolean multiCurrencyEnabled;

    /**
     * Person account record type ID
     */
    private static Id personAccountRecordTypeId;

    /**
     * Holds the last DB exception
     */
    public static Database.Error lastDBException;

    /**
     * Holds the standard price book
     */
    private static Pricebook2 stdPriceBook;

    /**
     * Holds the set of currencies
     */
    private static Set<String> currencies;


    static {
        isSandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        failForTest = 0;
        emailErrors = '';
        emailDebug = '';
        sendEmailOfErrors = false;
        isPersonAccountsEnabled = SObjectType.RecordType.fields.getMap().containsKey('IsPersonType');
        multiCurrencyEnabled = Schema.getGlobalDescribe().containsKey('CurrencyType');
    }

    /**
     * Gets the person account record type ID.
     * @return Person account record type ID.
     */
    public static Id getPersonAccountRecordType() {
        if (isPersonAccountsEnabled && personAccountRecordTypeId == null) {
            personAccountRecordTypeId = Schema.SObjectType.Account
                    .getRecordTypeInfosByDeveloperName().get('PersonAccount').getRecordTypeId();
        }
        return personAccountRecordTypeId;
    }

    /**
     * @return true if country/state picklists are enabled
     */
    public static Boolean isCountryPicklistEnabled() {
        return Contact.getSObjectType().getDescribe().fields.getMap().keySet().contains('mailingcountrycode');
    }

    /**
     * Get a list of currencies
     * @return Set{String} of currencies
     */
    public static Set<String> getCurrencies() {
        if (currencies == null) {
            currencies = new Set<String>();
            if (Utils.multiCurrencyEnabled) {
                List<SObject> currenciesQuery = Database.query(
                        'SELECT IsoCode FROM CurrencyType WHERE IsActive = TRUE'
                );
                for (SObject matchingCurrency : currenciesQuery) {
                    currencies.add((String) matchingCurrency.get('IsoCode'));
                }
            } else {
                currencies.add(UserInfo.getDefaultCurrency());
            }
        }
        return currencies;
    }

    /**
     * Checks if the input currency code exists in the SF currency list and returns true
     * @param currencyCode String with the ISO currency code
     * @return true if the currency code is valid in the SF configuration, always returns false if multicurrency is off
     */
    public static Boolean canSetCurrencyCode(String currencyCode) {
        if (Utils.multiCurrencyEnabled) {
            Set<String> currencySet = Utils.getCurrencies();
            return currencySet.contains(currencyCode);
        }
        return false;
    }

    /**
     * Sets a parameter to purposely fail tests or perform other behaviour in tests only, to increase code coverage.
     * @param val true if test flag is set
     */
    public static void setTestFlag(Boolean val) {
        failForTest = val ? 1 : 0;
    }

    /**
     * Works like {@link Utils#setTestFlag(Boolean)}, but you can specify multiple test flag indices.
     * @param i The index of the test flag
     */
    public static void setTestFlag(Integer i) {
        failForTest = i;
    }

    /**
     * Returns if test flag is set
     * @return Test flag value
     */
    public static Boolean getTestFlag() {
        return failForTest == 1;
    }

    /**
     * Returns test flag index i
     * @param i Test flag index
     * @return Test flag index i value
     */
    public static Boolean getTestFlag(Integer i) {
        return failForTest == i;
    }

    /**
     * Returns a map casted as a Map<String, Object> from a Map<String, Object>.
     * @param key Key
     * @param objMap Map<String, Object>
     * @return Map from objMap at key.
     */
    public static Map<String, Object> getMapFromMap(String key, Map<String, Object> objMap) {
        return (Map<String, Object>) objMap.get(key);
    }

    /**
     * Returns a list casted as a List<Object> from a Map<String, Object>.
     * @param key Key
     * @param objMap Map<String, Object>
     * @return List from objMap at key.
     */
    public static List<Object> getListFromMap(String key, Map<String, Object> objMap) {
        return (List<Object>) objMap.get(key);
    }

    /**
     * Returns the first Map<String, Object> (casted) from a list inside a Map<String, Object>.
     * @param key Key
     * @param objMap Map<String, Object>
     * @return The Map<String, Object> at index 0 of list inside objMap at key.
     */
    public static Map<String, Object> getFirst(String key, Map<String, Object> objMap) {
        List<Object> theObj = (List<Object>) objMap.get(key);
        return (Map<String, Object>) theObj[0];
    }

    /**
     * Returns a string formatted as yyyy-MM-ddTHH:mm:ss.SSSZ from a datetime object.
     * @param dt Datetime to format
     * @return Formatted string.
     */
    public static String formatDatetime(Datetime dt) {
        String dateTimeFormat = 'yyyy-MM-dd\'T\'HH:mm:ss.SSSZ';
        String timezone = 'UTC';//'America/New_York';
        System.debug(dt);
        return dt.format(dateTimeFormat, timezone);
    }


    /**
     * Takes a full ISO8601 timestamp and returns only the date.
     * @param dateString Datetime string from API.
     * @return Date separated from datetime.
     */
    public static String dateFromAPIString(Object dateString) {
        return String.valueOf(dateString).split('T')[0];
    }

    /**
     * Converts an ISO8601 timestamp into a datetime.
     * @param dateString ISO8601 timestamp
     * @return Datetime
     */
    public static Datetime apiStringToDatetime(Object dateString) {
        if (dateString == null) return null;

        Datetime parsedDt = (Datetime) JSON.deserialize('"' + dateString + '"', Datetime.class);
        return parsedDt;
    }

    /**
     * Same as {@link Utils#apiStringToDatetime(Object)}, but returns a date.
     * @param dateString ISO8601 timestamp
     * @return Date
     */
    public static Date apiStringToDate(Object dateString) {
        Datetime dtConverted = apiStringToDatetime(dateString);
        return dtConverted == null ? null :
                Date.newInstance(dtConverted.year(), dtConverted.month(), dtConverted.day());
    }

    /**
     * Adds a warning to the email debug buffer.
     * @param err Warning message.
     */
    public static void warning(String err) {
        System.debug(LoggingLevel.WARN, '--> [W] ' + err);
        emailDebug += '[W] ' + err + '\n';
        emailErrors += '[W] ' + err + '\n';
        //        sendEmailOfErrors = true;
    }

    /**
     * Adds an error to the email debug buffer.
     * @param err Error message.
     */
    public static void error(String err) {
        System.debug(LoggingLevel.ERROR, '--> [E] ' + err);
        emailDebug += '[E] ' + err + '\n';
        emailErrors += '[E] ' + err + '\n';
        sendEmailOfErrors = true;
    }

    /**
     * Adds a debug message to the email debug buffer.
     * @param str Debug message.
     */
    public static void log(String str) {
        System.debug(LoggingLevel.DEBUG, '--> [D] ' + str);
        emailDebug += '[D] ' + str + '\n';
    }

    /**
     * Verbosely logs a debug message.
     * @param str Debug message.
     */
    public static void logVerbose(String str) {
        if (!isSandbox) return;
        System.debug(LoggingLevel.DEBUG, '--> [V] ' + str);
        emailDebug += '[V] ' + str + '\n';
    }

    /**
     * Sends all logs to email.
     */
    public static void sendErrorLogEmail(String emailParam) {
        if (emailErrors == '' || !sendEmailOfErrors) return;
        System.debug('We are sending email errors: ' + emailErrors.length());

        String emailBody = 'Organization info:\n' +
                'Org name (Id): ' + UserInfo.getOrganizationName() + ' (' + UserInfo.getOrganizationId() + ')' +
                '\nUser name (Id): ' + UserInfo.getName() + ' (' + UserInfo.getUserId() + ')' +
                '\nUser email: ' + UserInfo.getUserEmail() +
                '\nIs sandbox? ' + isSandbox +
                ' / Is multi currency? ' + multiCurrencyEnabled +
                ' / Is person accounts? ' + isPersonAccountsEnabled +
                '\n\nError log:\n' + emailErrors + '\n\nFull debug log:\n' + emailDebug;
        sendSingleMail('QLead / Error', emailBody, emailParam);
    }


    /**
     * Sends an email.
     * @param subject Email subject
     * @param errorMessage Email body
     * @param emailParam Email address
     */
    public static void sendSingleMail(String subject, String errorMessage, String emailParam) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String[] toAddresses;
            toAddresses = new String[]{emailParam};

        // TODO: Change this to OWEA
        // OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'dre@qlead.io'];
        // if (owea.size() > 0) {
        //     email.setOrgWideEmailAddressId(owea.get(0).Id);
        // }

        mail.setToAddresses(toAddresses);
        mail.setReplyTo('dev@qlead.io');
        mail.setSenderDisplayName('QLead Support');
        //mail.setSubject('Error from Org : ' + UserInfo.getOrganizationName());
        mail.setSubject(subject);
        mail.setPlainTextBody(errorMessage);

        if (getTestFlag()) mail = null;
        try {
            //            if (!getTestFlag()) // !isSandbox ||
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        } catch (Exception e) {
            System.debug('Fatal error: ' + e.getMessage());
        }
    }


    /**
     * Sends an error to the org's default user
     */
    public static void sendUserError(String error) {
        String toAddress = UserInfo.getUserEmail();
        String emailBody = 'This is an error report from the DMS installed on your organization '
                + UserInfo.getOrganizationName() + ' (' + UserInfo.getOrganizationId() + ')\n\n' + error;
        sendSingleMail('Quantum Lead: Error', emailBody, toAddress);
    }

    /**
     * Sends a notification to the org's default user
     */
    public static void sendUserNotification(String message) {
        String toAddress = UserInfo.getUserEmail();
        String emailBody = 'This is a notification from the DMS installed on your organization '
                + UserInfo.getOrganizationName() + ' (' + UserInfo.getOrganizationId() + ')\n\n' + message;
        sendSingleMail('Quantum Lead: Notification', emailBody, toAddress);
    }

    /**
     * Custom HTTP Callout Mock, used for tests.
     */
    public class EchoMock implements HttpCalloutMock {
        public String resp { get; set; }
        public Integer respcode { get; set; }

        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(resp);
            res.setStatusCode(respcode == null ? 200 : respcode);
            return res;
        }
    }

    /**
     * Database operations.
     */
    public enum DBOperation {
        DBINSERT, DBUPDATE, DBUPSERT, DBDELETE
    }

    /**
     * Perform a database operation and handle errors.
     * @param operation Refer to {@link Utils#DBOperation}
     * @param sObjectList List of sObjects to perform DB Operation on.
     * @param sObjectField For upserts only, the field on which the upsert should be performed
     * @return true if all rows were successful
     */
    public static Boolean recordsOperation(DBOperation operation, List<SObject> sObjectList, SObjectField sObjectField) {
        Boolean allSuccessful = true;

        // Convert SObjects into concrete type first
        Schema.SObjectType sObjectType = sObjectList.getSObjectType();

        if (sObjectType == null) return null;

        String listType = 'List<' + sObjectType + '>';
        List<SObject> sObjects = (List<SObject>) Type.forName(listType).newInstance();
        for (SObject theSobject : sObjectList) {
            sObjects.add(theSobject);
        }

        if (operation == DBOperation.DBUPSERT) {
            Database.UpsertResult[] result;
            try {
                if (sObjectField == null) {
                    result = Database.upsert(sObjects, false);
                }
                else {
                    result = Database.upsert(sObjects, sObjectField, false);
                }
            } catch (Exception e) {
                error('Unhandled exception while upserting records ' + e.getMessage());
            }

            for (Integer i = 0; i < result.size(); i++) {
                Database.UpsertResult row = result[i];
                if (!row.isSuccess() || getTestFlag()) {
                    error('Error in upserting sobject: ' + row.getErrors());
                    error('Details about sobject: ' + sObjects[i]);
                    if (row.getErrors().size() > 0) {
                        lastDBException = row.getErrors()[0];
                    }
                    allSuccessful = false;
                }
                else {
                    logVerbose('Upserted ' + result[i] + ' to db');
                }
            }
        }
        else if (operation == DBOperation.DBINSERT || operation == DBOperation.DBUPDATE) {
            Database.SaveResult[] result;

            try {
                if (operation == DBOperation.DBINSERT) {
                    result = Database.insert(sObjects, false);
                }
                else {
                    result = Database.update(sObjects, false);
                }
            } catch (Exception e) {
                error('Unhandled exception while inserting/updating records ' + e.getMessage());
            }

            for (Integer i = 0; i < result.size(); i++) {
                Database.SaveResult row = result[i];
                if (!row.isSuccess() || getTestFlag()) {
                    error('Error in inserting/updating sobject: ' + row.getErrors());
                    error('Details about sobject: ' + sObjects[i]);
                    if (row.getErrors().size() > 0) {
                        lastDBException = row.getErrors()[0];
                    }
                    allSuccessful = false;
                }
                else {
                    logVerbose('Inserted/updated ' + result[i] + ' to db');
                }
            }
        }
        else if (operation == DBOperation.DBDELETE) {
            Database.DeleteResult[] result;
            try {
                result = Database.delete(sObjects, false);
            } catch (Exception e) {
                error('Unhandled exception while deleting records ' + e.getMessage());
            }

            for (Integer i = 0; i < result.size(); i++) {
                Database.DeleteResult row = result[i];
                if (!row.isSuccess() || getTestFlag()) {
                    error('Error in deleting sobject: ' + row.getErrors());
                    error('Details about sobject: ' + sObjects[i]);
                    if (row.getErrors().size() > 0) {
                        lastDBException = row.getErrors()[0];
                    }
                    allSuccessful = false;
                }
                else {
                    logVerbose('Deleted ' + result[i] + ' from db');
                }
            }
        }

        return allSuccessful;
    }

    /**
     * Same as {@link Utils#recordsOperation(DBOperation, List<SObject>, SObjectField}, but with no SObjectField
     * @param operation Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>, SObjectField}
     * @param sObjects Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>, SObjectField}
     * @return Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>, SObjectField}
     */
    public static Boolean recordsOperation(DBOperation operation, List<SObject> sObjects) {
        return recordsOperation(operation, sObjects, null);
    }

    /**
     * Same as {@link Utils#recordsOperation(DBOperation, List<SObject>}, but with one object only
     * @param operation Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>}
     * @param sObjectParam Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>}
     * @return Refer to {@link Utils#recordsOperation(DBOperation, List<SObject>}
     */
    public static Boolean recordsOperation(DBOperation operation, SObject sObjectParam) {
        Schema.SObjectType sObjectType = sObjectParam.getSObjectType();
        if (sObjectType == null) return null;

        String listType = 'List<' + sObjectType + '>';
        List<SObject> sObjects = (List<SObject>) Type.forName(listType).newInstance();
        sObjects.add(sObjectParam);

        return recordsOperation(operation, sObjects, null);
    }

    /**
     * Compares an API last modified string against the sobject's last modified datetime.
     * @param apiLastModified API last modified string.
     * @param sobjectLastmodified SObject last modified datetime.
     * @param objectId SObject ID (for debugging)
     * @return true if API object is more recent than the sObject.
     */
    public static Boolean checkLastModified(String apiLastModified, Datetime sobjectLastmodified, Object objectId) {
        // We want to check the last modified timestamp to see if this needs to be updated
        Datetime lastModified = apiStringToDatetime(apiLastModified);
        //System.debug('API Last modified: ' + lastModified + ' / System: ' + sobjectLastmodified + ' / String: ' + apiLastModified);
        if (lastModified <= sobjectLastmodified) {
            log('Skipping object ' + objectId + ' because API lastModified <= system lastModified | API Last modified: ' + lastModified + ' / System: ' + sobjectLastmodified + ' / String: ' + apiLastModified);
            return false;
        }
        return true;
    }

    /**
     * Converts a list of SObjects to a map, where the key is specified by fieldName
     * @param sobjects List of SObjects
     * @param fieldName Field to use as the key for hte map
     * @return Map of the SObjects with the key as per fieldName
     */
    public static Map<String, SObject> mapFromSObjectsByField(List<SObject> sobjects, String fieldName) {
        Schema.SObjectType sObjectType = sobjects.getSObjectType();

        if (sObjectType != null) {
            String mapType = 'Map<String, ' + sObjectType + '>';
            Map<String, SObject> mapOfSobjects = (Map<String, SObject>) Type.forName(mapType).newInstance();
            for (SObject theSobject : sobjects) {
                mapOfSobjects.put(String.valueOf(theSobject.get(fieldName)), theSobject);
            }
            return mapOfSobjects;
        }
        return null;
    }

    /**
     * Checks if a given address is valid as per Salesforce's validation.
     * @param emailAddress Email address to validate
     * @return true if the email is valid
     */
    public static Boolean isValidEmail(String emailAddress) {
        // RegEx is from the official SF Documentation
        // https://developer.salesforce.com/docs/atlas.en-us.noversion.mc-apis.meta/mc-apis/
        // using_regular_expressions_to_validate_email_addresses.htm
        String expression = '^[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9]' +
                '(?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$';

        return Pattern.matches(expression, emailAddress);
    }

    /**
     * Return all fields of given object
     * @param objectName The name of the SObject
     * @param additionalFields Any other fields, comma separated
     * @return SELECT FIELDS(ALL) FROM OBjectName
     */
    public static String getSelectAllFieldsString(String objectName, String additionalFields) {
        List<String> fields = new List<String>(Schema.getGlobalDescribe().get(objectName).getDescribe().fields
                .getMap().keySet());
        String query = 'SELECT ' + String.join(fields, ',') + additionalFields + ' FROM ' + objectName;
        return query;
    }

    /**
     * Same as above, but wihtout the additional fields.
     * @param objectName The name of the SObject
     * @return SELECT FIELDS(ALL) FROM ObjectName
     */
    public static String getSelectAllFieldsString(String objectName) {
        return getSelectAllFieldsString(objectName, '');
    }
}