/*
 * Created by rnavr on 30-09-2019.
 */
@IsTest class TestUtils {
    @TestSetup static void init() {
    }

    @IsTest static void testFailForTest() {
        Test.startTest();

        Utils.setTestFlag(true);
        System.assertEquals(true, Utils.getTestFlag(),  'getTestFlag() should be true');
        Utils.setTestFlag(false);
        Test.stopTest();
    }

    @IsTest static void testFailForTestInts() {
        Test.startTest();

        Utils.setTestFlag(3);
        System.assertEquals(true, Utils.getTestFlag(3), 'getTestFlag(3) should be true');
        Utils.setTestFlag(false);
        Test.stopTest();
    }


    @IsTest static void testMapFieldFunctions() {
        Test.startTest();
        // These are some methods that were never used but I'm keeping them just in case we need them for the future
        Map<String, Object> testo = new Map<String, Object>();
        Map<String, String> testx = new Map<String, String>();
        List<Map<String, String>> test2 = new List<Map<String, String>>();
        testx.put('testx', 'testing');
        test2.add(testx);
        testo.put('test', testx);
        testo.put('test2', test2);
        Map<String, Object> test1Res = Utils.getMapFromMap('test', testo);
        List<Map<String, String>> test2Res = (List<Map<String, String>>) Utils.getListFromMap('test2', testo);
        Map<String, String> test2Res1st = (Map<String, String>) test2Res[0];
        System.assertEquals('testing', String.valueOf(test1Res.get('testx')), 'getFieldMap failed');
        System.assertEquals('testing', test2Res1st.get('testx'), 'getArray failed');
        System.assertEquals('testing',
                String.valueOf(((Map<String, String>) Utils.getFirst('test2', testo)).get('testx')),
                'getFirst failed');
        Test.stopTest();
    }

    @IsTest static void testDtFunctions() {
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email = 'testingalot@testorg.com',
                EmailEncodingKey = 'UTF-8', LastName = 'Testing', LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US', ProfileId = p.Id,
                TimeZoneSidKey = 'Africa/Algiers', Username = 'testingalot@testorg.com');
        // Algiers does not observe DST, is +1

        System.runAs(u) {
            Datetime dt = Datetime.newInstance(2019, 1, 2, 3, 0, 0);
            String formattedDt = Utils.formatDatetime(dt);
            Datetime reconvertedDt = Utils.apiStringToDatetime(formattedDt);
            Date reconvertedDate = Utils.apiStringToDate(formattedDt);
            System.assertEquals('2019-01-02T02:00:00.000+0000', formattedDt, 'Datetime did not convert properly');
            System.assertEquals(3, reconvertedDt.hour(), 'API String timezone information did not convert properly');
            System.assertEquals(2, reconvertedDate.day(), 'API String day should be 2');
        }
        System.assertEquals('2019-01-01', Utils.dateFromAPIString('2019-01-01T00:00:00.000+0000'),
                'Did not get correct date from API string');
        Test.stopTest();
    }

    @IsTest static void testLogging() {
        Utils.setTestFlag(true);
        Test.startTest();
        Utils.error('test');
        Utils.log('test');
        Utils.logVerbose('test');
        Test.stopTest();
    }

    @IsTest static void testDateErrors() {
        Utils.setTestFlag(true);
        Test.startTest();
        Datetime dt = System.now();
        Utils.apiStringToDatetime('2019-01-01');
        String formattedDt = Utils.formatDatetime(dt);
        Datetime reconvertedDt = Utils.apiStringToDatetime(formattedDt);
        Date reconvertedDate = Utils.apiStringToDate(formattedDt);
        Test.stopTest();
    }


    @IsTest static void testEmailErrors() {
        Test.startTest();
        Utils.warning('Test');
        Utils.error('Test');
        Utils.sendErrorLogEmail('test@test.com');
        Test.stopTest();
    }

    @IsTest static void testUserEmails() {
        Test.startTest();
        Utils.sendUserError('error');
        Utils.sendUserNotification('notification');
        Test.stopTest();
    }

    @IsTest static void testEmailErrorsWithoutAnyEmails() {
        Test.startTest();
        Utils.warning('Test');
        Utils.error('Test');
        Utils.sendErrorLogEmail('test@test.com');
        Test.stopTest();
    }

    @IsTest static void testEmailErrorsFailure() {
        Utils.setTestFlag(true);
        Test.startTest();
        Utils.error('Test');
        Utils.sendErrorLogEmail('test@test.com');
        Test.stopTest();
    }

    @IsTest static void testRecordsOperations() {
        Test.startTest();
        List<Account> testAcc = new List<Account>{new Account(Name = 'test')};
        Utils.recordsOperation(Utils.DBOperation.DBINSERT, testAcc);

        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'test'];
        Utils.recordsOperation(Utils.DBOperation.DBUPDATE, testAcc);
        Utils.recordsOperation(Utils.DBOperation.DBUPSERT, testAcc);
        Utils.recordsOperation(Utils.DBOperation.DBDELETE, testAcc);
        Utils.recordsOperation(Utils.DBOperation.DBINSERT, new Account(Name = 'test'));
        Test.stopTest();
    }

    @IsTest static void testRecordsOperationsFail() {
        Test.startTest();
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        List<Account> testAcc = new List<Account>{new Account(Name = 'test')};
        Utils.recordsOperation(Utils.DBOperation.DBINSERT, testAcc);

        Utils.setTestFlag(true);
        testAcc = [SELECT Id, Name FROM Account WHERE Name = 'test'];
        List<Account> testAcc2 = new List<Account>{new Account(Name = 'test2')};
        Utils.recordsOperation(Utils.DBOperation.DBINSERT, testAcc2);
        Utils.recordsOperation(Utils.DBOperation.DBUPDATE, testAcc);
        Utils.recordsOperation(Utils.DBOperation.DBUPSERT, testAcc);
        Utils.recordsOperation(Utils.DBOperation.DBDELETE, testAcc);
        Test.stopTest();
    }

    @IsTest static void testCheckLastModified() {
        Boolean didItWork = Utils.checkLastModified('2000-01-01T00:00:00.000+0000', System.now(), null);
        System.assertEquals(false, didItWork, 'Last modified check should have been true');
        didItWork = Utils.checkLastModified('2099-01-01T00:00:00.000+0000', System.now(), null);
        System.assertEquals(true, didItWork, 'Last modified check should have been true, unless it\'s past 2099, in which case someone needs to update this test');
    }

    /*@IsTest static void testDreConfigs() {
        Utils.getDREConfigs();
        DRE_Config__c test = Utils.getFirstDREConfig();
        System.assertEquals('https://localhost', test.Store_URL__c, 'store url is wrong');
    }*/

    @IsTest static void testMapFromSObjectsByField() {
        List<OpportunityStage> test = [SELECT Id, MasterLabel FROM OpportunityStage LIMIT 3];
        Map<String, OpportunityStage> testMap = (Map<String, OpportunityStage>) Utils.mapFromSObjectsByField(test, 'MasterLabel');

        String firstTestKey = test[0].MasterLabel;
        System.assertEquals(test[0].Id, testMap.get(firstTestKey).Id, 'key-value mismatch');
    }

    @IsTest static void testEmailValidator() {
        String validEmail = 'test@gmail.com';
        String invalidEmail = 'test,test@gfail.com';
        System.assertEquals(true, Utils.isValidEmail(validEmail));
        System.assertEquals(false, Utils.isValidEmail(invalidEmail));
    }

    @IsTest static void coverChecks() {
        Utils.isCountryPicklistEnabled();
        Utils.getPersonAccountRecordType();
        Utils.getCurrencies();
        Utils.canSetCurrencyCode('USD');

        // TODO: Write a proper test for this
        Utils.getSelectAllFieldsString('Contact');
    }

    @IsTest static void testMock() {
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        HttpRequest req = new HttpRequest();
        req.setEndpoint('localhost');
        (new Http()).send(req);
    }
}