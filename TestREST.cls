@IsTest private class TestREST {
    @TestSetup static void init() {
    }

    @IsTest static void testHTTP() {
        Test.startTest();
        REST rest = new REST('localhost', true);
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 200;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedGet('test/', mock);
        Test.stopTest();
    }

    @IsTest static void testHTTPAuth() {
        Test.startTest();
        REST restObj = new REST('localhost');
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 200;
        Test.setMock(HttpCalloutMock.class, mock);
        restObj.request('localhost', REST.RequestMethod.PUT, 'test', 'Key', 'Pass', 'application/json', '1000');
        Test.stopTest();
    }

    @IsTest static void testHTTP500() {
        REST rest = new REST('localhost');
        Test.startTest();
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedGet('test/', mock);
        Test.stopTest();
    }

    @IsTest static void testHTTPFail() {
        Utils.setTestFlag(true);
        REST rest = new REST('localhost');
        Test.startTest();
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedGet('test/', mock);
        Test.stopTest();
    }

    @IsTest static void testRestPostHelper() {
        Test.startTest();
        REST rest = new REST('localhost');
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 200;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedPost('test/', new Map<String,Object>{}, mock);
        Test.stopTest();
    }

    @IsTest static void testRestPostHelperFail() {
        Utils.setTestFlag(true);
        REST rest = new REST('localhost');
        Test.startTest();
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedPost('test/', new Map<String,Object>{}, mock);
        Test.stopTest();
    }

    @IsTest static void testRestPutHelper() {
        Test.startTest();
        REST rest = new REST('localhost');
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 200;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedPut('test/', new Map<String,Object>{}, mock);
        Test.stopTest();
    }

    @IsTest static void testRestPutHelperFail() {
        Utils.setTestFlag(true);
        REST rest = new REST('localhost');
        Test.startTest();
        Utils.EchoMock mock = new Utils.EchoMock();
        mock.resp = '';
        mock.respcode = 500;
        Test.setMock(HttpCalloutMock.class, mock);
        rest.loggedPut('test/', new Map<String,Object>{}, mock);
        Test.stopTest();
    }

}