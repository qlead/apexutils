@IsTest private class TestSettingsManager {
    @IsTest static void testInsertSetting() {
        SettingsManager.setValue('Test', 'val');
        System.assertEquals('val', SettingsManager.getValue('Test', 'noval'));

        // Test for cache as well
        System.assertEquals('val', SettingsManager.getValue('Test', 'noval'));
    }

    @IsTest static void testUpdateSetting() {
        SettingsManager.setValue('Test', 'val');
        System.assertEquals('val', SettingsManager.getValue('Test', 'noval'));

        SettingsManager.setValue('Test', 'val2');
        System.assertEquals('val2', SettingsManager.getValue('Test', 'noval'));
        // Test for cache as well
        System.assertEquals('val2', SettingsManager.getValue('Test', 'noval'));
    }

    @IsTest static void testDefaultValue() {
        System.assertEquals('noval', SettingsManager.getValue('Test', 'noval'));
        System.assertEquals('noval', SettingsManager.getValue('Test', 'noval'));
    }

    @IsTest static void testException() {
        SettingsManager.SETTINGS_OBJECT_NAME = 'FakeObject';
        SettingsManager.setValue('Test', 'val2');
        System.assertNotEquals(null, SettingsManager.getException());
    }

    @IsTest static void testInvocable() {
        SettingsManager.setValue('Test', 'val');
        System.assertEquals('val', SettingsManager.getValueInvocable(new List<String>{'Test'})[0]);
    }
}