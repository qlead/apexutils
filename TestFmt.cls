@IsTest private class TestFmt {
    @IsTest static void testBehavior() {
        System.assertEquals('Test 1', Fmt.str('Test {0}', 1));
        System.assertEquals('Test 1ab', Fmt.str('Test {0}{1}', 1, 'ab'));
        System.assertEquals('Test 1ab' + System.now(), Fmt.str('Test {0}{1}{2}', 1, 'ab', System.now()));
        System.assertEquals('Test 1abefcd', Fmt.str('Test {0}{1}{3}{2}', 1, 'ab', 'cd', 'ef'));
        System.assertEquals('Test 1abefcdgh', Fmt.str('Test {0}{1}{3}{2}{4}', 1, 'ab', 'cd', 'ef', 'gh'));
        System.assertEquals('Test 1abefcdghij', Fmt.str('Test {0}{1}{3}{2}{4}{5}', 1, 'ab', 'cd', 'ef', 'gh', 'ij'));
        System.assertEquals('Test 1abefcdghij2', Fmt.str('Test {0}{1}{3}{2}{4}{5}{6}', 1, 'ab', 'cd', 'ef', 'gh', 'ij', 2));
        System.assertEquals('Test 1abefcdghij2a', Fmt.str('Test {0}{1}{3}{2}{4}{5}{6}{7}', 1, 'ab', 'cd', 'ef', 'gh', 'ij', 2, 'a'));
    }
}